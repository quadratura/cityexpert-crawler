export enum EnumTagTypes {
  GENERIC = 'generic', //  For all generic tag types
  LANDLOARD = 'landlord', // Agency, Owner etc..
  FLOOR = 'floor', // Determine on which floor flat is located
  PURCHASE_OPTION = 'purchase_option', // Rend or sell
  FURNISHED_STATUS = 'furnished_status',
  NUMBER_OF_ROOMS = 'number_of_rooms',
  HEATING = 'heating',
  PROPERTY_TYPE = 'property_type', // House, apartment, room
  LOCATION_ADDRESS = 'location_address',
  PROPERTY_CONDITION = 'property_condition',
  PARKING = 'parking',
}

export enum EnumFurnishedStatus {
  NOT_FURNISHED = 'not_furnished',
  SEMI_FURNISHED = 'semi_furnished',
  FULLY_FURNISHED = 'fully_furnished',
}
export enum EnumHeatingType {
  HEATING_PUMP = 'heating_pump',
  ELECTRIC_HEATING = 'electric_heating',
  STOREY_HEATER = 'storey_heater',
  FLOOR_HEATING = 'floor_heating',
  STORAGE_HEATER = 'storage_heater',
  CENTRAL_HEATING = 'central_heating',
}
export enum EnumLandlord {
  AGENCY = 'agency',
  OWNER = 'owner',
}
export enum EnumParking {
  GARAGE_PARKING = 'garage_parking',
  FREE_ZONE_PARKING = 'free_zone_parking',
  PRIVATE_PARKING = 'private_parking',
  ZONE_PARKING = 'zone_parking',
  BUILDING_PARKING = 'building_parking',
}
export enum EnumPropertyCondition {
  LUX = 'lux',
  RENOVATED = 'renovated',
  ORIGINAL = 'original',
  REQUIRES_RENOVATION = 'requires_renovation',
}
export enum EnumPurchaseOption {
  RENT = 'rent',
  SELL = 'sell',
}
export enum EnumPropertyType {
  HOUSE = 'house',
  APARTMENT = 'apartment',
  APARTMENT_IN_HOUSE = 'apartment_in_house',
  SHOP = 'shop',
  OFFICE_SPACE = 'office_space',
}

export interface IFlat {
  text: string;
  url: string;
  source: {
    url: string;
    name: string;
  };
  geo_location: {
    type: 'Point';
    coordinates: [number, number];
  };
  images: { url: string }[];
  size: string | undefined;
  price: string | undefined;
  tags: { text: string; kind: EnumTagTypes }[];
}
