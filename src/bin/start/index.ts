import { CronJob } from 'cron';
import http from 'http';
import axios from 'axios';
import Crawler from '../../services/crawler';
import { SCHEDULER_CRON, APP_PORT, QUEUE_HEALTHCHECK_URL } from '../../config';

function startHealthCheckServer() {
  http
    .createServer(function(req, res) {
      res.write('OK');
      res.end();
    })
    .listen(APP_PORT);
}

function startScheduler() {
  new CronJob(SCHEDULER_CRON, () => {
    const crawler = new Crawler();
    axios.get(QUEUE_HEALTHCHECK_URL);
    crawler.run().then(() => console.log('Done'));
  }).start();
}

(function main() {
  startHealthCheckServer();
  startScheduler();
})();
