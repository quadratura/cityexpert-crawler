import axios from 'axios';
import { IFlat, EnumTagTypes, EnumLandlord } from '../../interfaces';
import { CITYEXPERT_IMG_CDN, CITYEXPERT_BASE_URL } from '../../config';
import stringToEnum from './stringToEnum';

export default class FlatFactory {
  private dictionary?: any;

  public constructor(
    private client = axios.create({
      baseURL: CITYEXPERT_BASE_URL,
    }),
    private imageCdnBaseUrl: string = CITYEXPERT_IMG_CDN
  ) {}

  private async getDictionary(): Promise<any> {
    if (this.dictionary) {
      return this.dictionary;
    }
    const {
      data: { sr },
    } = await this.client.get('/api/resources/langKeys', {
      headers: {
        accept: 'application/json, text/plain, */*',
      },
    });
    this.dictionary = sr;

    return this.dictionary;
  }

  private async resolveKey(prefix: string, id: string): Promise<string> {
    const dict = await this.getDictionary();

    return String(dict[`${prefix}${id}`]).trim();
  }

  private async resolveKeys(key: string, arrayIDs: any[]): Promise<string[]> {
    const resolved: string[] = [];

    for (let id of arrayIDs) {
      resolved.push(await this.resolveKey(key, String(id)));
    }

    return resolved.filter(Boolean);
  }

  private async resolveUrl(
    id: string,
    structure: string,
    street: string,
    municipality: string,
    rentOrSale: 's' | 'r'
  ): Promise<string> {
    const dictionary = await this.getDictionary();
    const floor = dictionary[`STR-${structure}`];

    const textPart = [floor, street, municipality]
      .map((part: string) => part.trim().toLowerCase())
      .join(' ')
      .replace(/ /g, '-');

    const leaseType = rentOrSale === 's' ? 'prodaja' : 'izdavanje';

    //cityexpert.rs/izdavanje/stan/33852/trosoban-terazije-stari-grad
    return `${CITYEXPERT_BASE_URL}/${leaseType}/stan/${id}/${textPart}`;
  }

  private resolveFullImageURL(relativeImage: string) {
    return `${this.imageCdnBaseUrl}/sites/default/files/styles/1920x/public/image/${relativeImage}`;
  }

  private resolveCoordinates(coords: string): [number, number] {
    const latLng = coords
      .split(',')
      .map(split => split.trim())
      .map(split => Number(split));
    return [latLng[1], latLng[0]];
  }

  private stringToEnum(input: string): string {
    return stringToEnum(input);
  }

  public async fromApiResponse(apiFlat: any): Promise<IFlat> {
    const heating = await this.resolveKeys('HEATING-', apiFlat.heatingArray);
    const furnishingExtra = await this.resolveKeys(
      'FURNISHING-',
      apiFlat.furnishingArray
    );
    const extras = await this.resolveKeys(
      'PROP-BLDG-EQ-',
      apiFlat.bldgOptsArray
    );
    const parkingTags = await this.resolveKeys(
      'PROP-PARKING-',
      apiFlat.parkingArray
    );
    return {
      source: {
        url: CITYEXPERT_BASE_URL,
        name: 'CityExpert',
      },
      text: apiFlat.street,
      url: await this.resolveUrl(
        apiFlat.propId,
        apiFlat.structure,
        apiFlat.street,
        apiFlat.municipality,
        apiFlat.rentOrSale
      ),
      geo_location: {
        type: 'Point',
        coordinates: this.resolveCoordinates(apiFlat.location),
      },
      images: [{ url: this.resolveFullImageURL(apiFlat.coverPhoto) }],
      size: String(apiFlat.size),
      price: String(apiFlat.price),
      tags: [
        {
          kind: EnumTagTypes.FURNISHED_STATUS,
          text: this.stringToEnum(
            await this.resolveKey('FURNISHED-', String(apiFlat.furnished))
          ),
        },
        {
          kind: EnumTagTypes.FLOOR,
          text: String(apiFlat.floor)
            .trim()
            .split('_')[0],
        },
        {
          kind: EnumTagTypes.PURCHASE_OPTION,
          text: this.stringToEnum(apiFlat.rentOrSale),
        },
        {
          kind: EnumTagTypes.LANDLOARD,
          text: EnumLandlord.AGENCY,
        },
        {
          kind: EnumTagTypes.PROPERTY_TYPE,
          text: this.stringToEnum(
            await this.resolveKey('PROPTYPE-', String(apiFlat.ptId))
          ),
        },
        {
          kind: EnumTagTypes.NUMBER_OF_ROOMS,
          text: String(apiFlat.structure),
        },
        ...heating.map(text => ({
          kind: EnumTagTypes.HEATING,
          text: this.stringToEnum(text),
        })),
        ...furnishingExtra.map(text => ({
          kind: EnumTagTypes.GENERIC,
          text: this.stringToEnum(text),
        })),
        ...parkingTags.map(text => ({
          kind: EnumTagTypes.PARKING,
          text: this.stringToEnum(text),
        })),
        ...extras.map(text => ({
          kind: EnumTagTypes.GENERIC,
          text,
        })),
      ],
    };
  }
}

// export enum ITagTypes {
//     GENERIC = "generic", //  For all generic tag types
//     PAYMENT_METHOD = "payment_method", // monthly, yearly etc
//     DESCRIPTION = "description",
//     LOCATION_ADDRESS = "location_address",
//     LOCATION_NEIGHBORHOOD = "location_neighborhood",
//     PROPERTY_CONDITION = "property_condition"

//     NUMBER_OF_ROOMS = "number_of_rooms",
//     PROPERTY_TYPE = "property_type", // House, apartment, room
//     LANDLOARD = "landlord", // Agency, Owner etc..
//     PURCHASE_OPTION = "purchase_option", // Rend or sell
//     HEATING = "heating",
//     LOCATION_DISTRICT = "location_district",
//     LOCATION_STREET = "location_street",
//     LOCATION_CITY = "location_city",
//     PROVIDER_URL = "provider_url",
//     FLOOR = "floor", // Determine on which floor flat is located
//     FLOORS_TOTAL = "floors_total", //
//     FURNISHED_STATUS = "furnished_status",
//   }
// {
//     "uniqueID": "28225-BS",
//     "propId": 28225,
//     "statusId": 51,
//     "cityId": 1,
//     "location": "44.82182, 20.45573",
//     "street": "Cara Uroša",
//     "floor": "2_4",
//     "size": 211,
//     "structure": "5+",
//     "municipality": "Stari grad",
//     "polygons": [
//       "Stari grad",
//       "Dorćol",
//       "Centar"
//     ],
//     "ptId": 1,
//     "price": 330000.0,
//     "coverPhoto": "cityexpert.rs_-_nekretnina_id_-_28225_-_7836_-_20170824_0.jpg",
//     "rentOrSale": "s",
//     "caseId": 44867,
//     "caseType": "BS",
//     "underConstruction": false,
//     "filed": 2,
//     "furnished": 3,
//     "ceiling": 2,
//     "furnishingArray": [],
//     "bldgOptsArray": [],
//     "heatingArray": [
//       4
//     ],
//     "parkingArray": [
//       2,
//       3
//     ],
//     "yearOfConstruction": 2,
//     "joineryArray": [
//       2
//     ],
//     "petsArray": [],
//     "otherArray": [],
//     "availableFrom": "0001-01-01T00:00:00Z",
//     "firstPublished": "2019-12-27T13:32:49Z",
//     "pricePerSize": 1563.9811
//   },
