import axios from 'axios';
import { CITYEXPERT_BASE_URL } from '../../config';
import FlatFactory from './helpers';
import { save_flat } from '../api';

const DEFAULT_FILTERS: {} = {
  ptId: [],
  cityId: 1,
  currentPage: 1,
  resultsPerPage: 60,
  floor: [],
  avFrom: false,
  underConstruction: false,
  furnished: [],
  furnishingArray: [],
  heatingArray: [],
  parkingArray: [],
  petsArray: [],
  minPrice: null,
  maxPrice: null,
  minSize: null,
  maxSize: null,
  polygonsArray: [],
  searchSource: 'regular',
  sort: 'datedsc',
  structure: [],
  propIds: [],
  filed: [],
  ceiling: [],
  bldgOptsArray: [],
  joineryArray: [],
  yearOfConstruction: [],
  otherArray: [],
  numBeds: null,
  category: null,
  maxTenants: null,
  extraCost: null,
  numFloors: null,
  numBedrooms: null,
  numToilets: null,
  numBathrooms: null,
  heating: null,
  bldgEquipment: [],
  cleaning: null,
  extraSpace: [],
  parking: null,
  parkingIncluded: null,
  parkingExtraCost: null,
  parkingZone: null,
  petsAllowed: null,
  smokingAllowed: null,
  aptEquipment: [],
  site: 'SR',
};

export default class Crawler {
  public constructor(
    private defaultFilters = DEFAULT_FILTERS,
    private factory = new FlatFactory(),
    private client = axios.create({
      baseURL: CITYEXPERT_BASE_URL,
    })
  ) {}

  private getFilters(filters: {}) {
    return {
      ...this.defaultFilters,
      ...filters,
    };
  }

  private fetchRecords(page: number = 1): Promise<any> {
    return this.client.post(
      '/api/Search',
      this.getFilters({
        currentPage: page,
      })
    );
  }

  private async getTotalPages() {
    const {
      data: {
        info: { pageCount },
      },
    } = await this.fetchRecords();

    return pageCount;
  }

  public async run() {
    const totalPages = await this.getTotalPages();

    for (let i = 1; i <= totalPages; i++) {
      const {
        data: { result },
      } = await this.fetchRecords(i);
      for (let record of result) {
        const flat = await this.factory.fromApiResponse(record);
        const id = await save_flat(flat);
        console.log({ id });
      }
    }
  }
}
