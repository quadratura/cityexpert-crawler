import {
  EnumFurnishedStatus,
  EnumHeatingType,
  EnumLandlord,
  EnumPropertyCondition,
  EnumPurchaseOption,
  EnumPropertyType,
  EnumParking,
} from '../../interfaces';

export default function stringToEnum(text: string): string {
  switch (text.toLocaleLowerCase()) {
    // Rent Or Sale
    case 's':
      return EnumPurchaseOption.SELL;
    case 'r':
      return EnumPurchaseOption.RENT;

    // Landlord
    case 'agencija':
      return EnumLandlord.AGENCY;
    case 'vlasnik':
      return EnumLandlord.OWNER;

    // Furnished status
    case 'prazan':
    case 'prazno':
      return EnumFurnishedStatus.NOT_FURNISHED;
    case 'polunamešten':
    case 'polunamešteno':
      return EnumFurnishedStatus.SEMI_FURNISHED;
    case 'namešten':
    case 'namešteno':
      return EnumFurnishedStatus.FULLY_FURNISHED;

    // Heating
    case 'cg':
      return EnumHeatingType.CENTRAL_HEATING;
    case 'toplotna pumpa':
      return EnumHeatingType.HEATING_PUMP;
    case 'etažno':
    case 'eg':
    case 'gas':
      return EnumHeatingType.STOREY_HEATER;
    case 'ta peć':
    case 'kaljeva peć':
    case 'ta':
    case 'ta':
      return EnumHeatingType.STORAGE_HEATER;
    case 'podno':
      return EnumHeatingType.FLOOR_HEATING;
    case 'eg na struju':
    case 'norveški radijatori':
    case 'mermerni radijatori':
      return EnumHeatingType.ELECTRIC_HEATING;

    // Property type
    case 'lokal':
    case 'poslovni prostor':
      return EnumPropertyType.SHOP;
    case 'stan':
      return EnumPropertyType.APARTMENT;
    case 'stan u kući':
      return EnumPropertyType.APARTMENT_IN_HOUSE;
    case 'kuća':
      return EnumPropertyType.HOUSE;

    // Property Condition
    case 'za renoviranje':
      return EnumPropertyCondition.REQUIRES_RENOVATION;
    case 'renovirano':
      return EnumPropertyCondition.RENOVATED;
    case 'izvorno stanje':
      return EnumPropertyCondition.ORIGINAL;
    case 'lux':
      return EnumPropertyCondition.LUX;

    // Parking
    case 'garaža':
      return EnumParking.GARAGE_PARKING;
    case 'slobodna zona':
      return EnumParking.FREE_ZONE_PARKING;
    case 'privatni parking':
      return EnumParking.PRIVATE_PARKING;
    case 'zona':
      return EnumParking.ZONE_PARKING;
    case 'parking zgrade':
    case 'slobodan parking zgrade':
      return EnumParking.BUILDING_PARKING;

    // Unable to determinate enum from tag text
    default:
      return text;
  }
}
