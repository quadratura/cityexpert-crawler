import axios from 'axios';
import { IFlat } from '../../interfaces';
import {
  AUTH0_BASE_URL,
  AUTH0_CLIENT_ID,
  AUTH0_CLIENT_SECRET,
  AUTH0_AUDIENCE,
  MAX_AXIOS_TIMEOUT,
  API_URL,
} from '../../config';

async function getAccessToken() {
  const {
    data: { access_token },
  } = await axios.post(`${AUTH0_BASE_URL}/oauth/token`, {
    client_id: AUTH0_CLIENT_ID,
    client_secret: AUTH0_CLIENT_SECRET,
    audience: AUTH0_AUDIENCE,
    grant_type: 'client_credentials',
  });

  return access_token;
}

export const save_flat = async (flat: IFlat): Promise<string> => {
  try {
    const token = await getAccessToken();
    const { data: response } = await axios.post(
      API_URL,
      {
        operationName: 'SaveFlat',
        variables: { flat: JSON.stringify(flat) },
        query:
          'mutation SaveFlat($flat: String!) {save(flat_as_json: $flat) {id}}',
      },
      {
        timeout: MAX_AXIOS_TIMEOUT,
        headers: {
          authorization: `Bearer ${token}`,
          'content-type': 'application/json',
        },
      }
    );
    return response.data.save.id;
  } catch (e) {
    console.error({ message: e.response.data });
    return e;
  }
};
