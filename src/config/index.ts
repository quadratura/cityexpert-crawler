import dotenv from 'dotenv';
dotenv.config();

export const CITYEXPERT_BASE_URL = process.env.CITYEXPERT_BASE_URL;
export const CITYEXPERT_IMG_CDN = process.env.CITYEXPERT_IMG_CDN;
export const AUTH0_BASE_URL = process.env.AUTH0_BASE_URL;
export const AUTH0_CLIENT_ID = process.env.AUTH0_CLIENT_ID;
export const AUTH0_CLIENT_SECRET = process.env.AUTH0_CLIENT_SECRET;
export const AUTH0_AUDIENCE = process.env.AUTH0_AUDIENCE;
export const MAX_AXIOS_TIMEOUT = Number(process.env.MAX_AXIOS_TIMEOUT);
export const API_URL = process.env.API_URL;
export const SCHEDULER_CRON = process.env.SCHEDULER_CRON;
export const APP_PORT = process.env.APP_PORT;
export const QUEUE_HEALTHCHECK_URL = process.env.QUEUE_HEALTHCHECK_URL;
